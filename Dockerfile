FROM base/archlinux:latest

RUN pacman -Sy --noconfirm archlinux-keyring
RUN pacman -Syu --noconfirm
RUN pacman-db-upgrade
RUN pacman -S --noconfirm ca-certificates-mozilla
RUN pacman -S --noconfirm make texlive-bin texlive-core texlive-latexextra
RUN pacman -S --noconfirm imagemagick sudo
RUN pacman -S --noconfirm --asdeps wget base-devel cmake libjpeg-turbo libpng sdl_image yasm && wget "https://aur.archlinux.org/cgit/aur.git/snapshot/libbpg.tar.gz" && bsdtar xvf libbpg.tar.gz && chown -R nobody:nobody libbpg && cd libbpg && sudo -u nobody makepkg && pacman -U --noconfirm libbpg-*.pkg.* && cd .. && rm -Rf libbpg libbpg.tar.gz
RUN rm /var/cache/pacman/pkg/*
