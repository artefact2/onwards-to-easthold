INCLUDES=$(shell find . -type f -name "*.tex")

default: draft-guide.pdf draft-journal.pdf guide.pdf journal.pdf

draft-%.pdf: %.tex $(INCLUDES) gen-images
	xelatex -halt-on-error -jobname=`echo $@ | rev | cut -b5- | rev` "\def\isdraft{1}\input{$<}"
	xelatex -halt-on-error -jobname=`echo $@ | rev | cut -b5- | rev` "\def\isdraft{1}\input{$<}"

%.pdf: %.tex $(INCLUDES) gen-images
	xelatex -halt-on-error $<
	xelatex -halt-on-error $<

gen-images:
	cd img/proc && make

clean:
	rm -f {,img/proc/}*.{jpg,png,aux,log,pdf,nav,out,snm,toc}

.PHONY: clean bpgdec
