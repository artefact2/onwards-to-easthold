%!TEX TS-program = xelatex
%!TEX encoding = UTF-8 Unicode

\input{preamble}

\usepackage{framed}
\newenvironment{vf}{\begin{framed}\vspace*{-\partopsep}\vspace*{-\topsep}}{\end{framed}}

\begin{document}

\hbRootChapter{Contributing to the Journal: a Quick Guide}

\hbLettrined{T}{his guide} is a very brief introduction on how to
contribute to the Party Journal. You'll find a quick summary of the
most essential \LaTeX\space commands and some commands that are
specific to the journal. \LaTeX\space is not a WYSIWYG
(What-You-See-Is-What-You-Get) system. It's a programming language for
typesetting good-looking books. You type commands in a text file that
gets eventually compiled into a beautiful-looking PDF
document. Because it is old (\TeX\space was released in 1978) and
powerful, it can be difficult to get into.

\section{\LaTeX\space basics}

In \TeX, most commands start with a backslash (\verb/\/). Blocks (such
as command arguments) are usually delimited by brackets (\verb/{/ and
\verb/}/).

Some characters have special meaning, if you want to insert them in a
sentence they will have to be escaped by a leading backslash (like
this: \verb/\%/). They include: \#, \% and \&.

\subsection{Paragraphs and whitespace}

In \LaTeX, whitespace is ignored, except a double newline which starts
a new paragraph. A percent sign (\verb/%/) is used to insert comments.

\begin{vf}
\begin{verbatim}
% This will get ignored
Vivamus ante neque, % So will this
ullamcorper sed magna eget, % And this
aliquam    venenatis    ex.

Lorem ipsum dolor sit amet, 
consectetur adipiscing elit. 
Maecenas efficitur tortor ut 
lacus ornare condimentum porttitor 
vel nunc. Suspendisse ultricies 
eu odio hendrerit posuere.
\end{verbatim}

\noindent Vivamus ante neque,
ullamcorper sed magna eget,
aliquam   venenatis   ex.

Lorem ipsum dolor sit amet, 
consectetur adipiscing elit. 
Maecenas efficitur tortor ut 
lacus ornare condimentum porttitor 
vel nunc. Suspendisse ultricies 
eu odio hendrerit posuere.
\end{vf}

\subsection{Emphasis}

\begin{vf}
\begin{verbatim}
I couldn't believe what I saw. 
\emph{Am I going insane?}
\end{verbatim}

I couldn't believe what I saw. \emph{Am I going insane?}
\end{vf}

You can also use \verb/\textit{italic text...}/. For bolding text, use
\verb/\textbf{bold text...}/.

\subsection{Document divisions}

The commands below are self-explanatory.

\begin{verbatim}
\part{Part name…}
\chapter{Chapter name…}
\section{Section name…}
\subsection{Subsection name…}
\subsubsection{Subsubsection name…}
\end{verbatim}

You can also use a starred version, like
\verb/\section*{Non-numbered section…}/ if you don't want the section
to appear in the table of contents.

\subsection{Lists}

\subsubsection{Unnumbered lists}

\begin{vf}
\begin{verbatim}
\begin{itemize}
\item Sed sed mauris vel tellus auctor ;
\item Vivamus hendrerit aliquet ornare ;
\item Sed sit amet urna quam.
\end{itemize}
\end{verbatim}

\begin{itemize}
\item Sed sed mauris vel tellus auctor ;
\item Vivamus hendrerit aliquet ornare ;
\item Sed sit amet urna quam.
\end{itemize}
\end{vf}

\subsubsection{Numbered lists}

\begin{vf}
\begin{verbatim}
\begin{enumerate}
\item Sed sed mauris vel tellus auctor ;
\item Vivamus hendrerit aliquet ornare ;
\item Sed sit amet urna quam.
\end{enumerate}
\end{verbatim}

\begin{enumerate}
\item Sed sed mauris vel tellus auctor ;
\item Vivamus hendrerit aliquet ornare ;
\item Sed sit amet urna quam.
\end{enumerate}
\end{vf}

\subsubsection{Description lists}

\begin{vf}
\begin{verbatim}
\begin{description}
\item[Aliquam molestie] Sed sed mauris vel 
  tellus auctor ;
\item[Purus tempus] Vivamus hendrerit 
  aliquet ornare ;
\item[Pharetra] Sed sit amet urna quam.
\end{description}
\end{verbatim}

\begin{description}
\item[Aliquam molestie] Sed sed mauris vel tellus auctor lacinia ;
\item[Purus tempus] Vivamus hendrerit aliquet ornare ;
\item[Pharetra] Sed sit amet urna quam.
\end{description}
\end{vf}

\section{Journal-specific commands}

\subsection{Speaking in character}

For short passages of text (one paragraph or less), use
\verb/\asCharacter{text...}/. (Replace \verb/Character/ with your
character name, of course.)

\begin{vf}
\begin{verbatim}
This is the party talking. \asSteryx{And
this is me, talking as Steryx.}
\end{verbatim}

This is the party talking. \asSteryx{And
  this is me, talking as Steryx.}
\end{vf}

For longer passages (more than one paragraph), you must use instead
the \verb/longAsCharacter/ environment, like this:

\begin{vf}
\begin{verbatim}
\begin{longAsSteryx}
This is one paragraph with me talking. 
This is filler text just to make this
paragraph span more than one line.

This is another paragraph, still me 
doing the talking.
\end{longAsSteryx}
\end{verbatim}

\begin{longAsSteryx}
This is one paragraph with me talking. 
This is filler text just to make this
paragraph span more than one line.

This is another paragraph, still me
doing the talking.
\end{longAsSteryx}
\end{vf}

\subsection{Lettrines}

Lettrines are the big capital letters usually found at the beginning
of chapters.

\begin{vf}
\begin{verbatim}
\hbLettrine{C}{urabitur} sed justo 
volutpat, placerat turpis eget, porta 
tellus. Integer ornare et ligula ac ornare. 
Integer eget varius leo. Suspendisse 
vel lectus vitae tortor commodo venenatis. 
Pellentesque habitant morbi tristique 
senectus et netus et malesuada fames 
ac turpis egestas. 
\end{verbatim}

\hbLettrine{C}{urabitur} sed justo volutpat,
placerat turpis eget, porta tellus. 
Integer ornare et ligula ac ornare. 
Integer eget varius leo. Suspendisse 
vel lectus vitae tortor commodo venenatis. 
Pellentesque habitant morbi tristique 
senectus et netus et malesuada fames 
ac turpis egestas. 
\end{vf}

If the lettrine has too much depth and seeps into the line below, use
\verb/\hbLettrined{}{}/ instead.

\begin{vf}
\begin{verbatim}
\hbLettrine{Q}{uisque} consequat…
\end{verbatim}

\hbLettrine{Q}{uisque} consequat scelerisque orci, eget porttitor
libero feugiat ut. Pellentesque habitant morbi tristique senectus et
netus et malesuada fames ac turpis egestas. Duis convallis fringilla
ligula vehicula mollis. Proin sit amet vestibulum sem.
\end{vf}

\begin{vf}
\begin{verbatim}
\hbLettrined{Q}{uisque} consequat…
\end{verbatim}

\hbLettrined{Q}{uisque} consequat scelerisque orci, eget porttitor
libero feugiat ut. Pellentesque habitant morbi tristique senectus et
netus et malesuada fames ac turpis egestas. Duis convallis fringilla
ligula vehicula mollis. Proin sit amet vestibulum sem.
\end{vf}

\subsection{Counting days}

Counting days is easy, thanks to counters. They count themselves
automatically, assuming the commands below are used properly.

Use \verb/\thenextday/ to pass a day and begin a new paragraph.

\begin{vf}
\begin{verbatim}
\thenextday Curabitur sed justo volutpat…

\thenextday Suspendisse vel lectus vitae…
\end{verbatim}

\thenextday Curabitur sed justo volutpat, placerat turpis eget, porta
tellus. Integer ornare et ligula ac ornare. Integer eget varius leo.

\thenextday Suspendisse vel lectus vitae tortor commodo
venenatis. Pellentesque habitant morbi tristique senectus et netus et
malesuada fames ac turpis egestas.
\end{vf}

If you want to pass time without starting a new paragraph, use the
\verb/\passday/ or \verb/\passday{number of days...}/ macros.

\begin{vf}
\begin{verbatim}
\passday
\thenextday Mauris ac bibendum sapien…

\passdays{30}
\thenextday Aliquam imperdiet justo vel…
\end{verbatim}

\passday \thenextday Mauris ac bibendum sapien, ac tincidunt
urna. Proin dolor purus, dictum eu dignissim non, egestas id
felis.

\passdays{30} \thenextday Aliquam imperdiet justo vel mattis
dignissim. Sed sit amet ullamcorper mi, eu ultricies sem. Sed non arcu
euismod, elementum dolor feugiat, pharetra enim.
\end{vf}

If you just want to get the current day without passing time at all,
use \verb/\theday/.

\begin{vf}
\begin{verbatim}
What day are we today? 
We're on day \theday.
\end{verbatim}

What day are we today? 
We're on day \theday.
\end{vf}

If you want to start a chapter with \verb/\thenextday/, use instead
the macro \verb/\lettrinethenextday/.

\begin{vf}
\begin{verbatim}
\lettrinethenextday Sed a ante nisl…
\end{verbatim}

\lettrinethenextday Sed a ante nisl. Pellentesque viverra, massa in
tincidunt dapibus, eros nunc dignissim ante, vitae fringilla risus
nulla volutpat augue. Aenean non molestie ex. Donec nec fringilla
erat. Pellentesque porta fermentum enim eget ornare.
\end{vf}

\subsection{Notes}

Notes are floating elements. As such, \emph{they will not necessarily
  be placed in the document where they appear in the source text. They
  can float above or below it, even to a different page.}

To write notes, use either the \verb/hbNote/, \verb/hbNote2/,
\verb/hbNoteWide/ or \verb/hbNoteWide2/ environments. You can use an
optional argument which gives \LaTeX\space an indication on where it
should put the note, if possible:

\begin{itemize}
\item \verb/t/ will try to put the note at the top of the page (or column), this is the default for wide notes ;
\item \verb/b/ will try to put the note at the bottom of the page (or column) ;
\item \verb/h/ will try to put the note approximately here, this is the default for non-wide notes ;
\item \verb/p/ will try to put the note on a separate page (with perhaps other floats like other notes or tables).
\end{itemize}

\begin{vf}
\begin{verbatim}
\begin{hbNote}
\subsubsection{This is a Note}
Maecenas vestibulum facilisis quam…
\end{hbNote}
\end{verbatim}
\end{vf}

\begin{hbNote}
\subsubsection{This is a Note}
Maecenas vestibulum facilisis quam a sodales. Etiam ac sodales mi. Nunc eget lacus eget ante aliquam posuere. Nunc eu justo id metus tincidunt suscipit condimentum eget augue.
\end{hbNote}

\begin{vf}
\begin{verbatim}
% This is how you use
% the optional argument
% Possible values: t, b, h, p
\begin{hbNote2}[b]
\subsubsection{This Note should be
at the bottom of the page}
Maecenas vestibulum facilisis quam…
\end{hbNote2}
\end{verbatim}
\end{vf}

\begin{hbNote2}[b]
\subsubsection{This Note should be at the bottom of the page}
Maecenas vestibulum facilisis quam a sodales. Etiam ac sodales mi. Nunc eget lacus eget ante aliquam posuere. Nunc eu justo id metus tincidunt suscipit condimentum eget augue.
\end{hbNote2}

\begin{vf}
\begin{verbatim}
\begin{hbNoteWide}[p]
\subsubsection{This Note should be
on a separate page}

\begin{multicols}{2}
Maecenas vestibulum facilisis quam…
\end{multicols}
\end{hbNote2}
\end{verbatim}
\end{vf}

\begin{hbNoteWide}[p]
\subsubsection{This Note should be
on a separate page}

\begin{multicols}{2}
  Maecenas vestibulum facilisis quam a sodales. Etiam ac sodales
  mi. Nunc eget lacus eget ante aliquam posuere. Nunc eu justo id
  metus tincidunt suscipit condimentum eget augue. Etiam a imperdiet
  nibh. Aliquam blandit, orci ac sollicitudin luctus, tortor dui
  elementum sapien, a porta leo dui eget lacus. Proin molestie
  molestie est, a molestie lectus feugiat id. Praesent dignissim
  luctus sapien, ac gravida lorem elementum id. Curabitur faucibus sed
  neque sed gravida. Ut molestie pretium eros, quis sagittis justo
  vehicula quis.

  Sed semper faucibus elit, eget posuere justo dignissim id. Ut orci
  nunc, tincidunt quis lectus ac, imperdiet efficitur
  turpis. Pellentesque habitant morbi tristique senectus et netus et
  malesuada fames ac turpis egestas. Nam vel tellus non leo gravida
  posuere eget nec augue. Morbi at nibh tincidunt, rutrum lectus ut,
  iaculis felis. Duis sit amet elit at nunc dictum aliquet ut vel
  neque. Cras blandit bibendum arcu, eget euismod enim condimentum sit
  amet. Donec fermentum hendrerit dolor, sed feugiat nisi ornare sit
  amet. Fusce cursus, nulla non vulputate finibus, justo nunc eleifend
  neque, eget posuere metus odio eget mi. Morbi at bibendum
  metus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis
  nec augue at nibh ullamcorper ornare. Vestibulum iaculis libero
  nibh, in accumsan dui tincidunt eget.
\end{multicols}
\end{hbNoteWide}

\subsection{Tables}

Tables are also floating elements, so they behave very similarly to
notes. To insert a table, use one of the \verb/hbNakedTable/,
\verb/hbNarrowTable/, \verb/hbWideTable/, \verb/hbFancyTable/ or
\verb/hbFancyWideTable/ environments.

They all take three arguments (except \verb/hbNakedTable/ which
doesn't have a title): the fisrt is the title, the second is the
column specification, the third is the optional float position (same
as notes). You can use the following column types:

\begin{itemize}
\item \verb/L/ left-aligned column, as small as possible ;
\item \verb/C/ centered column, as small as possible ;
\item \verb/R/ right-aligned column, as small as possible ;
\item \verb/Y/ left-aligned column, fit width to fill available space ;
\item \verb/Z/ centered column, fit width to fill available space.
\end{itemize}

A table must have at least one \verb/Y/ or \verb/Z/ column (multiple
are also possible, in this case they will share the available space).

In the table environment, use \verb/&/ to separate cells and \verb/\\/
to start a new row.

\begin{vf}
\begin{verbatim}
\begin{hbNakedTable}{LZR}
\textbf{Left-aligned} & 
\textbf{Center-fill} &
\textbf{Right-aligned} \\
1st & Someone & 500 gp \\
2nd & Someone else & 250 gp \\
3rd & Another person & 100 gp \\
4th & etc. & 0 gp
\end{hbNakedTable}
\end{verbatim}

\begin{hbNakedTable}{LZR}
\textbf{Left-aligned} & 
\textbf{Center-fill} &
\textbf{Right-aligned} \\
1st & Someone & 500 gp \\
2nd & Someone else & 250 gp \\
3rd & Another person & 100 gp \\
4th & etc. & 0 gp
\end{hbNakedTable}
\end{vf}

\begin{vf}
\begin{verbatim}
\begin{hbNarrowTable}{Fancy Table
Example}{CY}
\textbf{Rank} & \textbf{Name} \\
1st & Nam Posuere \\
2nd & Felis Vitae \\
3rd & Eros eleifend
\end{hbNarrowTable}
\end{verbatim}
\end{vf}

\begin{hbFancyTable}{Fancy Table Example}{CY}
\textbf{Rank} & \textbf{Name} \\
1st & Nam Posuere \\
2nd & Felis Vitae \\
3rd & Eros eleifend
\end{hbFancyTable}

\begin{vf}
\begin{verbatim}
% Note the p float specifier
\begin{hbFancyWideTable}[p]{Fancy 
Wide Table Example}{YZZZZ}
\textbf{Name} & \textbf{Race} &
\textbf{Age} & \textbf{Height} &
\textbf{Eye color} \\
Augue & Elf & 71 & 5 ft 6 in & blue \\
Turpis & Human & 25 & 5 ft 11 in & brown \\
etc.
\end{hbFancyWideTable}
\end{verbatim}
\end{vf}

\begin{hbFancyWideTable}[p]{Fancy 
Wide Table Example}{YZZZZ}
\textbf{Name} & \textbf{Race} &
\textbf{Age} & \textbf{Height} &
\textbf{Eye color} \\
Augue & Elf & 71 & 5 ft 6 in & blue \\
Eleifend & Elf & 100+ & 5 ft 7 in & brown \\
Justo & Dwarf & 19 & 4 ft 10 in & brown \\
Pulvinar & Human & 21 & 6 ft & green \\
Turpis & Human & 25 & 5 ft 11 in & brown
\end{hbFancyWideTable}

It is also possible to do more complex tables with cells that span
multiple rows (or columns). Look in the source for examples, or just
ask me.

\subsection{Art}

Inserting art is tricky. Look in the source for examples, or just ask
me.

\subsection{Miscellaneous}

The \verb/\dead/ macro is for inserting the dead dagger symbol
(\dead).

\end{document}
