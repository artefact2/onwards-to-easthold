# Onwards to Easthold

**Read the party journal: [final
version](https://artefact2.gitlab.io/onwards-to-easthold/journal.pdf),
[draft
version](https://artefact2.gitlab.io/onwards-to-easthold/draft-journal.pdf).**

# Contribution guidelines

* [Read the guide](https://artefact2.gitlab.io/onwards-to-easthold/guide.pdf) ;

* Try to keep images around 120 DPI (1000x1414px for a full A4 page,
for example).
